{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

{-# OPTIONS_GHC -funbox-strict-fields #-}

module CuteC2.C where

import Prelude hiding (min, max)

import Data.Int
import Data.Word
import Data.Vector.Sized as V
import Foreign.C.Types
import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal.Array
import GHC.TypeLits
import Data.Proxy
import Data.Type.Equality
import CuteC2.Internal

#include <cute_c2.h>

-- 2d vector
data C2V = C2V
  { x :: Float
  , y :: Float
  } deriving (Eq, Show)

instance Storable C2V where
  alignment _ = #{alignment c2v}
  sizeOf _ = #{size c2v}

  peek p = do
    x <- #{peek c2v, x} p
    y <- #{peek c2v, y} p
    pure C2V{..}

  poke p C2V{..} = do
    #{poke c2v, x} p x
    #{poke c2v, y} p y

-- | Rotation. c and s are the "cos/sin pair for a single angle"
data C2R = C2R
  { c :: Float
  , s :: Float
  } deriving (Eq, Show)

instance Storable C2R where
  alignment _ = #{alignment c2r}
  sizeOf _ = #{size c2r}

  peek p = do
    c <- #{peek c2r, c} p
    s <- #{peek c2r, s} p
    pure C2R{..}

  poke p C2R{..} = do
    #{poke c2r, c} p c
    #{poke c2r, s} p s

-- | 2d transformation "x"
data C2X = C2X
  { p :: C2V -- ^ Position offset
  , r :: C2R -- ^ rotation
  } deriving (Eq, Show)

instance Storable C2X where
  alignment _ = #{alignment c2x}
  sizeOf _ = #{size c2x}

  peek ptr = do
    p <- #{peek c2x, p} ptr
    r <- #{peek c2x, r} ptr
    pure C2X{..}

  poke ptr C2X{..} = do
    #{poke c2x, p} ptr p
    #{poke c2x, r} ptr r

data C2Circle = C2Circle
  { p :: C2V
  , r :: Float
  } deriving (Eq, Show)

instance Storable C2Circle where
  alignment _ = #{alignment c2Circle}
  sizeOf _ = #{size c2Circle}

  peek ptr = do
    p <- #{peek c2Circle, p} ptr
    r <- #{peek c2Circle, r} ptr
    pure C2Circle{..}

  poke ptr C2Circle{..} = do
    #{poke c2Circle, p} ptr p
    #{poke c2Circle, r} ptr r

data C2AABB = C2AABB
  { min :: C2V
  , max :: C2V
  } deriving (Eq, Show)

instance Storable C2AABB where
  alignment _ = #{alignment c2AABB}
  sizeOf _ = #{size c2AABB}

  peek p = do
    min <- #{peek c2AABB, min} p
    max <- #{peek c2AABB, max} p
    pure C2AABB{..}

  poke p C2AABB{..} = do
    #{poke c2AABB, min} p min
    #{poke c2AABB, max} p max

data C2Capsule = C2Capsule
  { a :: C2V
  , b :: C2V
  , r :: Float
  } deriving (Eq, Show)

instance Storable C2Capsule where
  alignment _ = #{alignment c2Capsule}
  sizeOf _ = #{size c2Capsule}

  peek p = do
    a <- #{peek c2Capsule, a} p
    b <- #{peek c2Capsule, b} p
    r <- #{peek c2Capsule, r} p
    pure C2Capsule{..}

  poke p C2Capsule{..} = do
    #{poke c2Capsule, a} p a
    #{poke c2Capsule, b} p b
    #{poke c2Capsule, r} p r

type C2MaxPolygonVerts = #{const C2_MAX_POLYGON_VERTS}

data C2Poly = forall n. (KnownNat n, n <= C2MaxPolygonVerts) => C2Poly
  { count :: C2Nat n
  , verts :: Vector n C2V
  , norms :: Vector n C2V
  }

instance Eq C2Poly where
  (==)
    C2Poly{count = x'count, verts = x'verts, norms = x'norms}
    C2Poly{count = y'count, verts = y'verts, norms = y'norms} =
    case sameNat x'count y'count of
      Nothing -> False
      Just Refl -> Prelude.and
        [ V.toList x'verts == V.toList y'verts
        , V.toList x'norms == V.toList y'norms
        ]

deriving instance Show C2Poly

type C2Int = #{type int}

data C2Nat (n :: Nat) = C2Nat
instance KnownNat n => Show (C2Nat n) where
  show x = "C2Nat @" <> (show $ natVal x)

instance Storable C2Poly where
  alignment _ = #{alignment c2Poly}
  sizeOf _ = #{size c2Poly}
  peek ptr = do
    countVal :: C2Int <- #{peek c2Poly, count} ptr
    case someNatVal (fromIntegral countVal) of
      Nothing -> error "c2Poly count < 0"
      Just (SomeNat (_ :: Proxy n)) -> do
        let count = C2Nat @n
        case isLE count (C2Nat @C2MaxPolygonVerts) of
          Nothing -> error $ unwords
            ["c2Poly count >"
            , show (natVal (C2Nat @C2MaxPolygonVerts))
            ]
          Just Refl -> do
            let vertsPtr = #{ptr c2Poly, verts} ptr
            vertsList <- peekArray (fromIntegral countVal) vertsPtr
            let normsPtr = #{ptr c2Poly, norms} ptr
            normsList <- peekArray (fromIntegral countVal) normsPtr
            let norms = maybe (error "impossible") id $ V.fromList @n normsList
            let verts = maybe (error "impossible") id $ V.fromList @n vertsList
            pure C2Poly{..}

  poke ptr C2Poly{..} = do
    let countVal :: C2Int = fromIntegral (natVal count)
    #{poke c2Poly, count} ptr countVal
    let vertsPtr = #{ptr c2Poly, verts} ptr
    pokeArray vertsPtr (V.toList verts)
    let normsPtr = #{ptr c2Poly, norms} ptr
    pokeArray normsPtr (V.toList norms)

newtype C2Type a = C2Type Word8 deriving (Show, Eq, Num)

c2TypeCircle :: C2Type C2Circle
c2TypeCircle = #{const C2_TYPE_CIRCLE}

c2TypeAABB :: C2Type C2AABB
c2TypeAABB = #{const C2_TYPE_AABB}

c2TypeCapsule :: C2Type C2Capsule
c2TypeCapsule = #{const C2_TYPE_CAPSULE}

c2TypePoly :: C2Type C2Poly
c2TypePoly = #{const C2_TYPE_POLY}

data C2Ray = C2Ray
  { p :: C2V
  , d :: C2V
  , t :: Float
  } deriving (Eq, Show)

instance Storable C2Ray where
  alignment _ = #{alignment c2Ray}
  sizeOf _ = #{size c2Ray}

  peek ptr = do
    p <- #{peek c2Ray, p} ptr
    d <- #{peek c2Ray, d} ptr
    t <- #{peek c2Ray, t} ptr
    pure C2Ray{..}

  poke ptr C2Ray{..} = do
    #{poke c2Ray, p} ptr p
    #{poke c2Ray, d} ptr d
    #{poke c2Ray, t} ptr t

data C2Raycast = C2Raycast
  { t :: Float
  , n :: C2V
  } deriving (Eq, Show)

instance Storable C2Raycast where
  alignment _ = #{alignment c2Raycast}
  sizeOf _ = #{size c2Raycast}

  peek p = do
    t <- #{peek c2Raycast, t} p
    n <- #{peek c2Raycast, n} p
    pure C2Raycast{..}

  poke p C2Raycast{..} = do
    #{poke c2Raycast, t} p t
    #{poke c2Raycast, n} p n

-- This type maps exactly to the C struct. It isn't meant to be
-- a user-facing type. The high-level bindings have a separate
-- sum type that captures that this can either be 1 or 2 points.
data C2Manifold = forall n. (KnownNat n, n <= 2) => C2Manifold
  { count :: C2Nat n
  , depths :: Vector n Float
  , contactPoints :: Vector n C2V
  , n :: C2V
  }

deriving instance Show C2Manifold

instance Storable C2Manifold where
  alignment _ = #{alignment c2Manifold}
  sizeOf _ = #{size c2Manifold}

  peek p = do
    countVal :: C2Int <- #{peek c2Manifold, count} p

    case someNatVal (fromIntegral countVal) of
      Nothing -> error "c2Manifold count < 0"
      Just (SomeNat (_ :: Proxy n)) -> do
        let count = C2Nat @n
        case isLE count (C2Nat @2) of
          Nothing -> error "c2Manifold count > 2"
          Just Refl -> do
            let depthsPtr = #{ptr c2Manifold, depths} p
            depthsList <- peekArray (fromIntegral countVal) depthsPtr
            let depths = maybe (error "impossible") id $ V.fromList @n depthsList
    
            let contactPointsPtr = #{ptr c2Manifold, contact_points} p
            contactList <- peekArray (fromIntegral countVal) contactPointsPtr
            let contactPoints = maybe (error "impossible") id $ V.fromList @n contactList

            n <- #{peek c2Manifold, n} p
            pure C2Manifold{..}

  poke p C2Manifold{..} = do
    let countVal :: C2Int = fromIntegral (natVal count)
    #{poke c2Manifold, count} p countVal
    let depthsPtr = #{ptr c2Manifold, depths} p
    pokeArray depthsPtr (V.toList depths)

    let contactPointsPtr = #{ptr c2Manifold, contact_points} p
    pokeArray contactPointsPtr (V.toList contactPoints)

    #{poke c2Manifold, n} p n

data C2TOIResult = C2TOIResult
  { hit :: Bool
  , toi :: Float
  , n :: C2V
  , p :: C2V
  , iterations :: #{type int}
  } deriving (Eq, Show)

instance Storable C2TOIResult where
  alignment _ = #{alignment c2TOIResult}
  sizeOf _ = #{size c2TOIResult}
  peek ptr = do
    hit <- #{peek c2TOIResult, hit} ptr
    toi <- #{peek c2TOIResult, toi} ptr
    n <- #{peek c2TOIResult, n} ptr
    p <- #{peek c2TOIResult, p} ptr
    iterations <- #{peek c2TOIResult, iterations} ptr
    pure C2TOIResult{..}
  poke ptr C2TOIResult{..} = do
    #{poke c2TOIResult, hit} ptr hit
    #{poke c2TOIResult, toi} ptr toi
    #{poke c2TOIResult, n} ptr n
    #{poke c2TOIResult, p} ptr p
    #{poke c2TOIResult, iterations} ptr iterations

foreign import ccall unsafe "cute_c2.h c2Collided" c2Collided
  :: Ptr a
  -> Ptr C2X
  -> C2Type a
  -> Ptr b
  -> Ptr C2X
  -> C2Type b
  -> Int

foreign import ccall unsafe "cute_c2.h c2CastRayPtr" c2CastRay
  :: Ptr C2Ray
  -> Ptr b
  -> Ptr C2X
  -> C2Type b
  -> Ptr C2Raycast
  -> IO Int

foreign import ccall unsafe "cute_c2.h c2ManifoldTEST" c2ManifoldTEST
  :: Ptr C2Manifold
  -> IO ()

foreign import ccall unsafe "cute_c2.h c2Collide" c2Collide
  :: Ptr a
  -> Ptr C2X
  -> C2Type a
  -> Ptr b
  -> Ptr C2X
  -> C2Type b
  -> Ptr C2Manifold
  -> IO ()

data C2GJKCache

foreign import ccall unsafe "cute_c2.h c2GJK" c2GJK
  :: Ptr a  -- ^ A
  -> C2Type a -- ^ typeA
  -> Ptr C2X -- ^ ax_ptr
  -> Ptr b  -- ^ B
  -> C2Type b -- ^ typeB
  -> Ptr C2X -- ^ bx_ptr
  -> Ptr C2V -- ^ outA
  -> Ptr C2V -- ^ outB
  -> CBool -- ^ use_radius
  -> Ptr #{type int} -- ^ iterations
  -> Ptr C2GJKCache
  -> IO Float

foreign import ccall unsafe "cute_c2.h c2TOIPtr" c2TOI
  :: Ptr a  -- ^ A
  -> C2Type a -- ^ typeA
  -> Ptr C2X -- ^ ax_ptr
  -> Ptr C2V -- ^ vA
  -> Ptr b  -- ^ B
  -> C2Type b -- ^ typeB
  -> Ptr C2X -- ^ bx_ptr
  -> Ptr C2V -- ^ vB
  -> CBool -- ^ use_radius
  -> Ptr C2TOIResult
  -> IO ()

foreign import ccall unsafe "cute_c2.h c2Inflate" c2Inflate
  :: Ptr a -- ^ shape
  -> C2Type a -- ^ type
  -> Float -- ^ skin_factor
  -> IO ()

foreign import ccall unsafe "cute_c2.h c2Hull" c2Hull
  :: Ptr C2V -- ^ verts
  -> #{type int} -- ^ count
  -> IO #{type int}

foreign import ccall unsafe "cute_c2.h c2Norms" c2Norms
  :: Ptr C2V -- ^ verts
  -> Ptr C2V -- ^ norms
  -> #{type int} -- ^ count
  -> IO ()
