{-# LANGUAGE StrictData #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedRecordDot #-}

{-# OPTIONS_GHC -funbox-strict-fields #-}

{-
TODO:
- CuteC2
  - Trim exports
  - Property tests for collision logic
    - Basically, testing cute_c2 but via these bindings
  - ST-trick to share allocations across calls
    - Benchmark re-using buffers
- CuteC2.C
  - Polys
  - c2GJK
  - c2TOI
  - c2MakePoly
-}
module CuteC2
  ( module CuteC2
  , C.C2Circle(..)
  , C.C2Capsule(..)
  , C.C2AABB(..)
  , C.C2Poly(..)
  , C.C2V(..)
  , C.C2X(..)
  , C.C2R(..)
  , C.C2Ray(..)
  , C.C2Raycast(..)
  , C.C2Nat(..)
  , C.C2TOIResult(..)
  ) where

import Prelude hiding (min, max)

import CuteC2.C hiding (C2Manifold (..))
import qualified CuteC2.C as C

import Data.Functor ((<&>))
import Control.Applicative (asum)
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils
import Foreign.Marshal.Array
import Foreign.Storable
import Foreign.Ptr
import System.IO.Unsafe
import Data.Vector.Sized as V
import Data.Finite (natToFinite)
import GHC.TypeLits
import GHC.Records
import Data.Type.Equality

data C2Shape =
    C2Circle' C.C2Circle
  | C2Capsule' C.C2Capsule
  | C2AABB' C.C2AABB
  | C2Poly' (Maybe C.C2X) C.C2Poly
  deriving (Eq, Show)

c2Circle :: C.C2V -> Float -> C2Shape
c2Circle p r = C2Circle' $ C.C2Circle{..}

c2AABB
  :: C.C2V -- ^ min
  -> C.C2V -- ^ max
  -> C2Shape
c2AABB mn mx = C2AABB' $ C.C2AABB mn mx

c2Capsule
  :: C.C2V -- ^ a
  -> C.C2V -- ^ b
  -> Float -- ^ r
  -> C2Shape
c2Capsule a b r = C2Capsule' $ C.C2Capsule a b r

c2Poly
  :: forall n
   . n <= C.C2MaxPolygonVerts
  => KnownNat n
  => Maybe C.C2X
  -> Vector n C.C2V -- ^ verts
  -> C2Shape
c2Poly mx verts = unsafePerformIO $ do
  let count = C.C2Nat @n
  let countC2 :: C.C2Int = fromIntegral (natVal count)
  let countI :: Int = fromIntegral (natVal count)
  withArray (V.toList verts) $ \pverts -> allocaArray countI $ \pnorms -> do
    C.c2Norms pverts pnorms countC2
    normsList <- peekArray countI pnorms
    let norms = maybe (error "impossible") id $ V.fromList @n normsList
    pure $ C2Poly' mx C.C2Poly{..}

c2xIdentity :: C2X
c2xIdentity = C2X (C2V 0 0) (C2R { c = 1.0, s = 0.0 })

c2xOffset :: C2V -> C2X -> C2X
c2xOffset c2v C2X{..} = C2X (c2Add p c2v) r

c2Add :: C2V -> C2V -> C2V
c2Add a b = C2V (a.x + b.x) (a.y + b.y)

c2Mul :: C2V -> C2V -> C2V
c2Mul a b = C2V (a.x * b.x) (a.y * b.y)

c2Move :: C.C2V -> C2Shape -> C2Shape
c2Move off = \case
  C2Circle' C2Circle{..} -> c2Circle (c2Add off p) r
  C2Capsule' C2Capsule{..} -> c2Capsule (c2Add off a) (c2Add off b) r
  C2AABB' C2AABB{..} -> c2AABB (c2Add off min) (c2Add off max)
  C2Poly' mc2x poly -> C2Poly' (Just $ maybe (c2xOffset off c2xIdentity) (c2xOffset off) mc2x) poly

-- TODO: Untested
c2RotateCapsule :: Float -> C2Capsule -> C2Capsule
c2RotateCapsule = undefined

c2FlipLeftAABB :: C2AABB -> C2AABB
c2FlipLeftAABB = undefined

storea :: Storable a => a -> (Ptr a -> IO r) -> IO r
storea a k = alloca $ \ptra -> do
  poke ptra a
  k ptra

shapea :: C2Shape -> (forall a. Ptr a -> Ptr C.C2X -> C.C2Type a -> IO r) -> IO r
shapea shape k = case shape of
  C2Circle' circle -> storea circle $ \pcircle ->
    k pcircle nullPtr C.c2TypeCircle
  C2Capsule' capsule -> storea capsule $ \pcapsule ->
    k pcapsule nullPtr C.c2TypeCapsule
  C2AABB' aabb -> storea aabb $ \paabb ->
    k paabb nullPtr C.c2TypeAABB
  C2Poly' mx poly -> storea poly $ \ppoly -> maybeWith with mx $ \pmx ->
    k ppoly pmx C.c2TypePoly

c2Collided :: C2Shape -> C2Shape -> Bool
c2Collided shape1 shape2 = unsafePerformIO $
  shapea shape1 $ \pshape1 pc2x1 ty1 -> shapea shape2 $ \pshape2 pc2x2 ty2 ->
    pure $ toBool $ C.c2Collided pshape1 pc2x1 ty1 pshape2 pc2x2 ty2

c2CastRay :: C.C2Ray -> C2Shape -> Maybe C.C2Raycast
c2CastRay ray shape = unsafePerformIO $
  storea ray $ \pray -> 
  shapea shape $ \pshape pc2x ty ->
  alloca @C.C2Raycast $ \out -> do
   res <- C.c2CastRay pray pshape pc2x ty out
   if res == 0 then pure Nothing else Just <$> peek out

data C2Manifold =
    C2Manifold'One
    { n :: C2V
    , depth1 :: Float
    , contactPoint1 :: C2V
    }
  | C2Manifold'Two
    { n :: C2V
    , depth1 :: Float, depth2 :: Float
    , contactPoint1 :: C2V, contactPoint2 :: C2V
    } deriving stock (Eq, Show)

instance HasField "depths" C2Manifold [Float] where
  getField = \case
    C2Manifold'One{..} -> [depth1]
    C2Manifold'Two{..} -> [depth1, depth2]

instance HasField "contactPoints" C2Manifold [C2V] where
  getField = \case
    C2Manifold'One{..} -> [contactPoint1]
    C2Manifold'Two{..} -> [contactPoint1, contactPoint2]

c2Collide :: C2Shape -> C2Shape -> Maybe C2Manifold
c2Collide x y = case c2CollideRaw x y of
  C.C2Manifold{..} -> asum
    [ sameNat (C2Nat @1) count <&> \Refl ->
        let depth1 = V.index depths (natToFinite $ C2Nat @0)
            contactPoint1 = V.index contactPoints (natToFinite $ C2Nat @0)
        in C2Manifold'One{..}
    , sameNat (C2Nat @2) count <&> \Refl ->
        let depth1 = V.index depths (natToFinite $ C2Nat @0)
            depth2 = V.index depths (natToFinite $ C2Nat @1)
            contactPoint1 = V.index contactPoints (natToFinite $ C2Nat @0)
            contactPoint2 = V.index contactPoints (natToFinite $ C2Nat @1)
        in C2Manifold'Two{..}
    -- It can only be 1 or 2, so we fallback to Nothing and assume it was 0
    ]

c2CollideRaw :: C2Shape -> C2Shape -> C.C2Manifold
c2CollideRaw shape1 shape2 = unsafePerformIO $
  shapea shape1 $ \pshape1 pc2x1 ty1 ->
  shapea shape2 $ \pshape2 pc2x2 ty2 ->
  alloca @C.C2Manifold $ \out -> do
    C.c2Collide pshape1 pc2x1 ty1 pshape2 pc2x2 ty2 out
    peek out

data C2GJKResult = C2GJKResult
  { c2GJK_outA :: C.C2V
  , c2GJK_outB :: C.C2V
  , c2GJK_distance :: Float
  } deriving (Eq, Show)

c2GJK
  :: C2Shape -- ^ a
  -> C2Shape -- ^ b
  -> Bool -- ^ use_radius
  -> C2GJKResult
c2GJK a b use_radius = unsafePerformIO $
  shapea a $ \pa ax tya -> shapea b $ \pb bx tyb ->
    alloca @C.C2V $ \outA -> alloca @C.C2V $ \outB -> do
      c2GJK_distance <- C.c2GJK pa tya ax pb tyb bx outA outB (fromBool use_radius) nullPtr nullPtr
      c2GJK_outA <- peek outA
      c2GJK_outB <- peek outB
      pure C2GJKResult{..}

c2TOI
  :: C2Shape -- ^ a
  -> C.C2V -- ^ vA
  -> C2Shape -- ^ b
  -> C.C2V -- ^ vB
  -> Bool -- ^ use_radius
  -> C.C2TOIResult
c2TOI a vA b vB use_radius = unsafePerformIO $
  shapea a $ \pa ax tya -> shapea b $ \pb bx tyb ->
    with vA $ \pvA -> with vB $ \pvB -> alloca $ \res -> do
      C.c2TOI pa tya ax pvA pb tyb bx pvB (fromBool use_radius) res
      peek res

c2Inflate :: Float -> C2Shape -> C2Shape
c2Inflate skin = unsafePerformIO . \case
  C2Circle' circle -> storea circle $ \p -> do
      C.c2Inflate p C.c2TypeCircle skin
      C2Circle' <$> peek p
  C2Capsule' capsule -> storea capsule $ \p -> do
      C.c2Inflate p C.c2TypeCapsule skin
      C2Capsule' <$> peek p
  C2AABB' aabb -> storea aabb $ \p -> do
      C.c2Inflate p C.c2TypeAABB skin
      C2AABB' <$> peek p
  C2Poly' mx poly -> storea poly $ \p -> do
    C.c2Inflate p C.c2TypePoly skin
    newPoly <- peek p
    pure $ C2Poly' mx newPoly
