#define CUTE_C2_IMPLEMENTATION
#include "cute_c2.h"

int c2CastRayPtr(c2Ray* A, const void* B, const c2x* bx, C2_TYPE typeB, c2Raycast* out)
{
  return c2CastRay(*A, B, bx, typeB, out);
}

void c2TOIPtr(const void* A, C2_TYPE typeA, const c2x* ax_ptr, c2v* vA, const void* B, C2_TYPE typeB, const c2x* bx_ptr, c2v* vB, int use_radius, c2TOIResult* out) {
  *out = c2TOI(A, typeA, ax_ptr, *vA, B, typeB, bx_ptr, *vB, use_radius);
}

void c2ManifoldTEST(c2Manifold* out)
{
  out->count = 11;
  out->depths[0] = 2.2;
  out->depths[1] = 3.3;
  out->contact_points[0] = (c2v){4.4,5.5};
  out->contact_points[1] = (c2v){6.6,7.7};
  out->n = (c2v){8.8,9.9};
  return;
}
